let prob = 1
let handle = 1
function handleRadio(x) {
    console.log('handle = '+x)
    handle = x
}
function Selector(sealLevel,Choice) {
    let multi = [1,2,3]
    let seal = [0.11,0.22,0.33,0.44,0.5,0.6,0.7,0.8,0.9,1]

    function calculate(seal,buff,multi){
        let rate = (seal * buff) + (seal * multi)
        console.log(rate)
        return rate
        // [Seal x Buff]+[Seal x Mul]
    }
    switch(Choice){
        // Normal
        case 1 : return calculate(seal[sealLevel],0,multi[0])
        // X2
        case 2 : return calculate(seal[sealLevel],0,multi[1])
        // X3
        case 3 : return calculate(seal[sealLevel],0,multi[2])
        // Faction
        case 4 : return calculate(seal[sealLevel],0.2,multi[0])
        // Faction + X2
        case 5 : return calculate(seal[sealLevel],0.2,multi[1])
        // Faction + X3
        case 6 : return calculate(seal[sealLevel],0.2,multi[2])
        default : console.log('error')

    }
}

function AmuletSuccess(){
   let choice = handle
    let rate = Selector(0,choice)
    // attack
    const set = {20:rate,0:prob-rate};
    console.log(prob-rate)
    let sum = 0
    for(let j in set){
        sum += set[j];
    }
    function pick_random(){
        var pick = Math.random()*sum;
        for(let j in set){
            pick -= set[j];
            if(pick <= 0){
                return j;
            }
        }
    }
    return parseInt(pick_random())
}

function StoneSuccess(){
    let choice = handle
     let rate = Selector(1/*seal number = level - 1*/,choice)
     // attack
     const set = {15:rate,0:prob-rate};
     console.log(prob-rate)
     let sum = 0
     for(let j in set){
         sum += set[j];
     }
     function pick_random(){
         var pick = Math.random()*sum;
         for(let j in set){
             pick -= set[j];
             if(pick <= 0){
                 return j;
             }
         }
     }
     return parseInt(pick_random())
 }

 function CrystalSuccess(){
    let choice = handle
     let rate = Selector(2/*seal number = level - 1*/,choice)
     // attack
     const set = {12:rate,0:prob-rate};
     console.log(prob-rate)
     let sum = 0
     for(let j in set){
         sum += set[j];
     }
     function pick_random(){
         var pick = Math.random()*sum;
         for(let j in set){
             pick -= set[j];
             if(pick <= 0){
                 return j;
             }
         }
     }
     return parseInt(pick_random())
 }

 function CalabashSuccess(){
    let choice = handle
     let rate = Selector(3/*seal number = level - 1*/,choice)
     // attack
     const set = {9:rate,0:prob-rate};
     console.log(prob-rate)
     let sum = 0
     for(let j in set){
         sum += set[j];
     }
     function pick_random(){
         var pick = Math.random()*sum;
         for(let j in set){
             pick -= set[j];
             if(pick <= 0){
                 return j;
             }
         }
     }
     return parseInt(pick_random())
 }

 function CrestSuccess(){
    let choice = handle
     let rate = Selector(4/*seal number = level - 1*/,choice)
     // attack
     const set = {7:rate,0:prob-rate};
     console.log(prob-rate)
     let sum = 0
     for(let j in set){
         sum += set[j];
     }
     function pick_random(){
         var pick = Math.random()*sum;
         for(let j in set){
             pick -= set[j];
             if(pick <= 0){
                 return j;
             }
         }
     }
     return parseInt(pick_random())
 }

 function RosarySuccess(){
    let choice = handle
     let rate = Selector(5/*seal number = level - 1*/,choice)
     // attack
     const set = {5:rate,0:prob-rate};
     console.log(prob-rate)
     let sum = 0
     for(let j in set){
         sum += set[j];
     }
     function pick_random(){
         var pick = Math.random()*sum;
         for(let j in set){
             pick -= set[j];
             if(pick <= 0){
                 return j;
             }
         }
     }
     return parseInt(pick_random())
 }

 function MirorSuccess(){
    let choice = handle
     let rate = Selector(6/*seal number = level - 1*/,choice)
     // attack
     const set = {4:rate,0:prob-rate};
     console.log(prob-rate)
     let sum = 0
     for(let j in set){
         sum += set[j];
     }
     function pick_random(){
         var pick = Math.random()*sum;
         for(let j in set){
             pick -= set[j];
             if(pick <= 0){
                 return j;
             }
         }
     }
     return parseInt(pick_random())
 }

 function NecklaceSuccess(){
    let choice = handle
     let rate = Selector(7/*seal number = level - 1*/,choice)
     // attack
     const set = {3:rate,0:prob-rate};
     console.log(prob-rate)
     let sum = 0
     for(let j in set){
         sum += set[j];
     }
     function pick_random(){
         var pick = Math.random()*sum;
         for(let j in set){
             pick -= set[j];
             if(pick <= 0){
                 return j;
             }
         }
     }
     return parseInt(pick_random())
 }

 function RingSuccess(){
    let choice = handle
     let rate = Selector(8/*seal number = level - 1*/,choice)
     // attack
     const set = {2:rate,0:prob-rate};
     console.log(prob-rate)
     let sum = 0
     for(let j in set){
         sum += set[j];
     }
     function pick_random(){
         var pick = Math.random()*sum;
         for(let j in set){
             pick -= set[j];
             if(pick <= 0){
                 return j;
             }
         }
     }
     return parseInt(pick_random())
 }

 function BoxSuccess(){
    let choice = handle
     let rate = Selector(9/*seal number = level - 1*/,choice)
     // attack
     const set = {1:rate,0:prob-rate};
     console.log(prob-rate)
     let sum = 0
     for(let j in set){
         sum += set[j];
     }
     function pick_random(){
         var pick = Math.random()*sum;
         for(let j in set){
             pick -= set[j];
             if(pick <= 0){
                 return j;
             }
         }
     }
     return parseInt(pick_random())
 }
export {AmuletSuccess,StoneSuccess,CrystalSuccess,CalabashSuccess,CrestSuccess,RosarySuccess,MirorSuccess,NecklaceSuccess,RingSuccess,BoxSuccess,handleRadio}