import React from 'react'
import './style.css'
import {Alert} from 'antd'
export default class WebFooter extends React.Component {
    render(){
        return(
            <div className="footer-text">

              <Alert message="Copyright 2019 KalimaPz-dev" type="info"></Alert>
              
            </div>
        )
    }
}