import React from 'react'
import Amulet from './Asset/SealAmulet.png'
import Stone from './Asset/SealStone.png'
import Crystal from './Asset/SealCrystal.png'
import Calabash from './Asset/SealCalabash.png'
import Crest from './Asset/SealCrest.png'
import Rosary from './Asset/SealRosary.png'
import Miror from './Asset/SealMiror.png'
import Necklace from './Asset/SealNecklace.png'
import Ring from './Asset/SealRing.png'
import Box from './Asset/SealBox.png'

import { handleRadio,
        AmuletSuccess,
        StoneSuccess,
        CrystalSuccess,
        CalabashSuccess,
        CrestSuccess,
        RosarySuccess,
        MirorSuccess,
        NecklaceSuccess,
        RingSuccess,
        BoxSuccess } 
        from './script/enchantment'

import { Row,
         Col,
         Modal,
         Radio,
         Alert,
         InputNumber,
         message,
         Button,
         Avatar, } 
         from 'antd'

import { Container } from 'reactstrap'
import './style.css'
import Footer from './WebFooter';

const margin = {
    margin : "5px"
}
const justify = {
    display : "flex",
    justifyContent : "center"
}

export default class Application extends React.Component {
    state = { visible: false };
    constructor(props){
        super(props)
        this.state = {
            slot : 14,
            leftSlot : 0,
            attackPower : 0,
            armor : 0,
            satan : 0,
            successCount: 0,
            failCount : 0
        }        
    }
    loadData = async => {}
    resetButtonModal = () =>{
        this.setState({
            visible: true,
          });
    }
    handleOk = e => {
        console.log(e);
        this.setState({
          visible: false,
        });
        this.resetButton()
      };
      handleCancel = e => {
        console.log(e);
        this.setState({
          visible: false,
        });
      };

    onCheckNormal = (value) => {
        console.log('Normal Select')
        message.success('อัตราการหลอมสำเร็จเป็น ปกติ')
        handleRadio(1)
    }
    onCheckX2 = (value) => {
        console.log('X2 Select')
        message.success('อัตราการหลอมสำเร็จเพิ่มขึ้น 2 เท่า')
        handleRadio(2)
    }
    onCheckX3 = (value) => {
        console.log('X3 Select')
        message.success('อัตราการหลอมสำเร็จเพิ่มขึ้น 3 เท่า')
        handleRadio(3)
    }
    onCheckF = (value) => {
        console.log('Faction Select')
        message.success('อัตราการหลอมสำเร็จเพิ่มขึ้น 20%')
        handleRadio(4)
    }
    onCheckF2 = (value) => {
        console.log('F2 Select')
        message.success('อัตราการหลอมสำเร็จเพิ่มขึ้น 2.2 เท่า')
        handleRadio(5)
    }
    onCheckF3 = (value) => {
        console.log('F3 Select')
        message.success('อัตราการหลอมสำเร็จเพิ่มขึ้น 3.2 เท่า')
        handleRadio(6)
    }
    
    handleOnChangeSlot = (value) => {
        this.setState({slot : value},()=>{this.loadData()})
        console.log('this.state.slot = '+this.state.slot)
    }

    decreaseSlot = () => {
        this.setState({slot : this.state.slot - 1})
    }

    upgradeByAmulet = () =>{  /// Rank1
        if(this.state.slot !== 0)
        {
            this.decreaseSlot()       
            let temp = AmuletSuccess()
            if (temp !== 0){
                this.setState({successCount : this.state.successCount + 1})
                message.success('หลอมสร้างสำเร็จ >_<')
            }   
            else {
                this.setState({failCount : this.state.failCount + 1})
                message.error('หลอมสร้างล้มเหลว T_T')
            }  
            this.setState({attackPower : this.state.attackPower + temp,armor : this.state.armor + (temp*2),satan : this.state.satan + (temp*4)})
            
        }
        else
        {
            message.error('จำนวนครั้งในการหลอมไม่พอ')
        }

    }
    upgradeByStone = () =>{  /// Rank2
        if(this.state.slot !== 0)
        {
            this.decreaseSlot()
            let temp = StoneSuccess()
            if (temp !== 0){
                this.setState({successCount : this.state.successCount + 1})
                message.success('หลอมสร้างสำเร็จ >_<หลอมสร้างสำเร็จ >_<')
            }   
            else {
                this.setState({failCount : this.state.failCount + 1})
                message.error('หลอมสร้างล้มเหลว T_T')
            }     
            this.setState({attackPower : this.state.attackPower + temp,armor : this.state.armor + (temp*2),satan : this.state.satan + (temp*4)})
        }
        else
        {
            message.error('จำนวนครั้งในการหลอมไม่พอ')  
        }
    }
    
    upgradeByCrystal = () =>{  /// Rank3
        if(this.state.slot !== 0)
        {
            this.decreaseSlot()       
            let temp = CrystalSuccess()
            if (temp !== 0){
                this.setState({successCount : this.state.successCount + 1})
                message.success('หลอมสร้างสำเร็จ >_<')
            }   
            else {
                this.setState({failCount : this.state.failCount + 1})
                message.error('หลอมสร้างล้มเหลว T_T')
            }  
            this.setState({attackPower : this.state.attackPower + temp,armor : this.state.armor + (temp*2),satan : this.state.satan + (temp*4)})
            
        }
        else
        {
            message.error('จำนวนครั้งในการหลอมไม่พอ')
        }
    }

    upgradeByCalabash = () =>{  /// Rank3
        if(this.state.slot !== 0)
        {
            this.decreaseSlot()       
            let temp = CalabashSuccess()
            if (temp !== 0){
                this.setState({successCount : this.state.successCount + 1})
                message.success('หลอมสร้างสำเร็จ >_<')
            }   
            else {
                this.setState({failCount : this.state.failCount + 1})
                message.error('หลอมสร้างล้มเหลว T_T')
            }  
            this.setState({attackPower : this.state.attackPower + temp,armor : this.state.armor + (temp*2),satan : this.state.satan + (temp*4)})
            
        }
        else
        {
            message.error('จำนวนครั้งในการหลอมไม่พอ')
        }
    }

    upgradeByCrest = () =>{  /// Rank3
        if(this.state.slot !== 0)
        {
            this.decreaseSlot()       
            let temp = CrestSuccess()
            if (temp !== 0){
                this.setState({successCount : this.state.successCount + 1})
                message.success('หลอมสร้างสำเร็จ >_<')
            }   
            else {
                this.setState({failCount : this.state.failCount + 1})
                message.error('หลอมสร้างล้มเหลว T_T')
            }  
            this.setState({attackPower : this.state.attackPower + temp,armor : this.state.armor + (temp*2),satan : this.state.satan + (temp*4)})
            
        }
        else
        {
            message.error('จำนวนครั้งในการหลอมไม่พอ')
        }
    }

    upgradeByRosary = () =>{  /// Rank3
        if(this.state.slot !== 0)
        {
            this.decreaseSlot()       
            let temp = RosarySuccess()
            if (temp !== 0){
                this.setState({successCount : this.state.successCount + 1})
                message.success('หลอมสร้างสำเร็จ >_<')
            }   
            else {
                this.setState({failCount : this.state.failCount + 1})
                message.error('หลอมสร้างล้มเหลว T_T')
            }  
            this.setState({attackPower : this.state.attackPower + temp,armor : this.state.armor + (temp*2),satan : this.state.satan + (temp*4)})
            
        }
        else
        {
            message.error('จำนวนครั้งในการหลอมไม่พอ')
        }
    }

    upgradeByMiror = () =>{  /// Rank3
        if(this.state.slot !== 0)
        {
            this.decreaseSlot()       
            let temp = MirorSuccess()
            if (temp !== 0){
                this.setState({successCount : this.state.successCount + 1})
                message.success('หลอมสร้างสำเร็จ >_<')
            }   
            else {
                this.setState({failCount : this.state.failCount + 1})
                message.error('หลอมสร้างล้มเหลว T_T')
            }  
            this.setState({attackPower : this.state.attackPower + temp,armor : this.state.armor + (temp*2),satan : this.state.satan + (temp*4)})
            
        }
        else
        {
            message.error('จำนวนครั้งในการหลอมไม่พอ')
        }
    }

    upgradeByNecklace = () =>{  /// Rank3
        if(this.state.slot !== 0)
        {
            this.decreaseSlot()       
            let temp = NecklaceSuccess()
            if (temp !== 0){
                this.setState({successCount : this.state.successCount + 1})
                message.success('หลอมสร้างสำเร็จ >_<')
            }   
            else {
                this.setState({failCount : this.state.failCount + 1})
                message.error('หลอมสร้างล้มเหลว T_T')
            }  
            this.setState({attackPower : this.state.attackPower + temp,armor : this.state.armor + (temp*2),satan : this.state.satan + (temp*4)})
            
        }
        else
        {
            message.error('จำนวนครั้งในการหลอมไม่พอ')
        }
    }

    upgradeByRing = () =>{  /// Rank3
        if(this.state.slot !== 0)
        {
            this.decreaseSlot()       
            let temp = RingSuccess()
            if (temp !== 0){
                this.setState({successCount : this.state.successCount + 1})
                message.success('หลอมสร้างสำเร็จ >_<')
            }   
            else {
                this.setState({failCount : this.state.failCount + 1})
                message.error('หลอมสร้างล้มเหลว T_T')
            }  
            this.setState({attackPower : this.state.attackPower + temp,armor : this.state.armor + (temp*2),satan : this.state.satan + (temp*4)})
            
        }
        else
        {
            message.error('จำนวนครั้งในการหลอมไม่พอ')
        }
    }

    upgradeByBox = () =>{  /// Rank3
        if(this.state.slot !== 0)
        {
            this.decreaseSlot()       
            let temp = BoxSuccess()
            if (temp !== 0){
                this.setState({successCount : this.state.successCount + 1})
                message.success('หลอมสร้างสำเร็จ >_<')
            }   
            else {
                this.setState({failCount : this.state.failCount + 1})
                message.error('หลอมสร้างล้มเหลว T_T')
            }  
            this.setState({attackPower : this.state.attackPower + temp,armor : this.state.armor + (temp*2),satan : this.state.satan + (temp*4)})
            
        }
        else
        {
            message.error('จำนวนครั้งในการหลอมไม่พอ')
        }
    }

    resetButton = () => {
        window.location.reload()
        this.setState({slot : 10})
        console.log('slot reset')
        console.log('slot count : '+ this.state.slot)
    }

    render(){
        
        return (
           <div>
           
            <div className="container-flex slot-box">
            <Container style={{paddingTop:"1%",paddingBottom:"1%"}}>
            <div className="header head-box"><h2>Soul Saver Composition Simulator</h2></div>
            </Container>
                <Container className="potential">
                    <Row className="potential">
                        <Col><Alert message={"พลังโจมตี : + "+ this.state.attackPower} type="error"/></Col>
                    </Row>    
                </Container>

                <Container>
                <Row className="potential">
                        <Col><Alert message={"พลังป้องกัน : + "+ this.state.armor} type="success"/></Col>
                    </Row> 
                </Container>
                <Container>
                <Row className="potential">
                        <Col><Alert message={"พลังโจมตีและป้องกัน เครื่องรางซานตานสถิตย์ : + "+ this.state.satan} type="warning"/></Col>
                    </Row> 
                </Container>
                <Container className="potential">
                    <Row gutter={8} className="potential">
                        <Col span={12}>
                            <Alert message={"อัพเกรดสำเร็จ : "+this.state.successCount+" ครั้ง"} type="success"/>
                        </Col>
                        <Col span={12}>
                        <Alert message={"อัพเกรดล้มเหลว : "+this.state.failCount+" ครั้ง"} type="error"/>
                        </Col>
                    </Row>
                    <Row className="potential">
                        <Col type="flex" justify="space-around" align="middle" className="slot-box">
                            จำนวนครั้งการหลอมสร้างทั้งหมด : <InputNumber size={'small'} min={0} max={15} defaultValue={this.state.slot} onChange={this.handleOnChangeSlot.bind(this)}></InputNumber>           
                        </Col> 
                    </Row>
                    <Row className="potential">
                        <Col type="flex" justify="space-around" align="middle" className="slot-box">
                        {"จำนวนครั้งการหลอมสร้างเหลือ "+ this.state.slot + " ครั้ง"}         
                        </Col> 
                    </Row>
                    <Row className="slot-box" gutter = {8}>
                        <Col>
                        <p>โหมดการหลอมสร้าง</p>
                        <Radio.Group name ="Mode" defaultValue={1}>
                                <p><Radio value={1} name="Normal" onChange={this.onCheckNormal}>ปกติ</Radio></p>
                                <p><Radio value={2} name="X2" onChange={this.onCheckX2}>ใช้ยันต์เสริมประกาย 2 เท่า (โอกาสสำเร็จเพิ่มขึ้น 2 เท่า)</Radio></p>
                                <p><Radio value={3} name="X3" onChange={this.onCheckX3}>ใช้ยันต์เสริมประกาย 3 เท่า (โอกาสสำเร็จเพิ่มขึ้น 3 เท่า)</Radio></p>
                                <p><Radio value={4} name="F" onChange={this.onCheckF}>ใช้บัพสงครามฝ่าย (โอกาสสำเร็จเพิ่มขึ้น 20%)</Radio></p>
                                <p><Radio value={5} name="F2" onChange={this.onCheckF2}>บัพสงครามฝ่าย + ยันต์เสริมประกาย 2 เท่า</Radio></p>
                                <p><Radio value={6} name="F3" onChange={this.onCheckF3}>บัพสงครามฝ่าย + ยันต์เสริมประกาย 3 เท่า</Radio></p>
                            </Radio.Group>
                        </Col>
                        
                     </Row>
                     <Container className="slot-box" style={margin}>
                <p>ผนึก</p>
                <div style={justify}>
                    <Button onClick={this.upgradeByAmulet} size="large" type="primary" style={margin}><Avatar src={Amulet}/></Button>
                    <Button onClick={this.upgradeByStone} size="large" type="primary" style={margin}><Avatar src={Stone}/></Button>
                    <Button onClick={this.upgradeByCrystal} size="large" type="primary" style={margin}><Avatar src={Crystal}/></Button>
                    <Button onClick={this.upgradeByCalabash} size="large" type="primary" style={margin}><Avatar src={Calabash}/></Button>
                    <Button onClick={this.upgradeByCrest} size="large" type="primary" style={margin}><Avatar src={Crest}/></Button>
                    <Button onClick={this.upgradeByRosary} size="large" type="primary" style={margin}><Avatar src={Rosary}/></Button>
                    <Button onClick={this.upgradeByMiror} size="large" type="primary" style={margin}><Avatar src={Miror}/></Button>
                    <Button onClick={this.upgradeByNecklace} size="large" type="primary" style={margin}><Avatar src={Necklace}/></Button>
                    <Button onClick={this.upgradeByRing} size="large" type="primary" style={margin}><Avatar src={Ring}/></Button>
                    <Button onClick={this.upgradeByBox} size="large" type="primary" style={margin}><Avatar src={Box}/></Button>
                </div>
            </Container>
                    <div className="reset-btn">
                     <Button type="danger" onClick={this.resetButtonModal}>คืนสภาพการหลอม</Button>
                     <Modal
                        title="Reset All Composition Value"
                        visible={this.state.visible}
                        onOk={this.handleOk} 
                        onCancel={this.handleCancel}  
                        >
                        <p>คืนสภาพการหลอม จะทำให้การหลอมสร้างทั้งหมดเป็นสถานะเริ่มต้น</p>
                        <p>ต้องการคืนสภาพการหลอมหรือไม่?</p>
                        
                    </Modal>
                    
                    </div>
                </Container>
                <Footer/>
                

                
            </div>
           </div>
        )
    }
}